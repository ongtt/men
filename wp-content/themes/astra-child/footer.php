<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<?php astra_content_bottom(); ?>
	</div> <!-- ast-container -->
	</div><!-- #content -->
<?php 
	astra_content_after();
		
	astra_footer_before();
		
	astra_footer();
		
	astra_footer_after(); 
?>
	</div><!-- #page -->
<?php 
	astra_body_bottom();    
	wp_footer(); 
?>
<?php $img_src =  get_template_directory_uri() . '/assets/'; ?>
<style>
/*     .intro-video {
    object-fit: cover;
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
		z-index: 999; */
}
</style>
<script>
(function ($) {
  $(document).ready(function ($) {
	 
    function setCookie(cname,cvalue,exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires=" + d.toGMTString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      console.log(ca);
      for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }




    var intro_html = 
        '<div class="pro introbox" >' +    
          '<video class="intro-video" autoplay muted id="myVideo">' +
            '<source src="https://menology.ducanhcao.com/wp-content/uploads/2022/09/Menology_Master_2K_070321_1500_2.mp4" type="video/mp4">' +
          '</video>' +
        '</div>'







    

    function checkCookie() {
      var intro=getCookie("intro");
      if (intro != "") {
        $('.hidehome').removeClass('hidehome');
        $('.introbox').remove();
      } else {

        $('#page').append(intro_html);
        setCookie("intro", "1", 1);
        setTimeout(function () {
          $('.hidehome').removeClass('hidehome');
          $('.introbox').fadeOut();

        }, 6000);
        
      }
    }

    //checkCookie();

    
  });
})(jQuery);
</script>
	</body>
</html>