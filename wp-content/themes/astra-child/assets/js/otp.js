(function ($) {
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split("&"),
      sParameterName,
      i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split("=");

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined
          ? true
          : decodeURIComponent(sParameterName[1]);
      }
    }
    return false;
  };
  $(document).ready(function () {
    $(".btn-otp").click(function () {
      let otp = $("#verificationCode").val();
      let code = getUrlParameter("code");
      $.ajax({
        type: "POST",
        dataType: "json",
        url: devvn_array1.admin_ajax,
        data: {
          action: "ajax_otp",
          otp: otp,
          code: code,
          nonce: devvn_array1.ajax_otp_nonce,
        },
        success: function (res) {
          console.log("200", res);
          console.log('xxx');
        //   if (res.data.status == 200) {
            window.location.href = "/";
        //   }
        },
        error: function (res) {
          console.log("-200", res);
        },
      });
    });
  });
})(jQuery);
