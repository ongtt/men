<?php
/**
 * Template Name: cs-otp
 *
 * Displays the cs-otp Template of the theme.
 */

get_header();

?>
<style>
   
input:focus, 
input[type="text"]:focus, 
input[type="email"]:focus, 
input[type="url"]:focus, 
input[type="password"]:focus, 
input[type="reset"]:focus, 
input[type="search"]:focus, 
textarea:focus {
  border-color: unset !important;
}
input:focus-visible {
  outline: none !important; 
}

.inputs input {
    width: 100%;
    height: 74px;
    font-size: 36px;
    padding: 0;
    text-align: center;
    border: 0;
    border-radius: 0 !important;
    border-bottom: 2px solid #D9D9D9;
    background-color: unset !important;
    margin-right: 10px;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0
}
#otp {
  text-align: center;
}
.wrap-form{
  width: 1170px;
  max-width: 100%;
  margin: 100px auto 120px;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
}
.wrap-form * {
  font-family: 'TT Commons';
}
.wrap-form > div{
  width: 50%;
}
.tit-wrap {
  margin-bottom: 30px;
}
.tit-wrap .tit {
  font-weight: 600;
  font-size: 42px;
  text-align: center;
}
.tit-wrap .content {
  font-weight: 400;
  font-size: 20px;
  text-align: center;
  line-height: 26px;
}
.btn-otp {
  background-color: #046D2F;
  padding: 10px 20px;
  display: flex;
  align-items: center;
}
.btn-otp svg {
    margin-left: 15px !important;
    display: none !important;
}
.btn-otp.active svg {
    display: block !important;
}
.btn-otp:hover {
  background-color: #046D2F;
}
.btn-resend-otp {
  background-color: unset !important;
  color: #000;
}
.btn-resend-otp:hover {
  color: #000;
}
.btn-wrap {
  margin-top: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
}
.from-content-right {
  background: #00000033;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 32px;
}
.from-content-right h3{
  font-weight: 600;
  font-size: 48px;
  line-height: 57px;
  letter-spacing: -0.01em;
  color: #FFFFFF;
}
.from-content-right p{
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  text-align: center;
  color: #FFFFFF;
}
.form-content-left {
    padding: 0 15px;
}
/*OTP PASSWORD*/
@media(max-width:767px){
    .tit-wrap > h3.tit{
    	font-size:28px;
    	line-height:33px
    }
    .wrap-form > div{
    	width:100%!important;
    }
	.tit-wrap .content{
	    font-size:14px!important;
		margin-top:8px;
		line-height:21px!important;
	}
	.wrap-form .inputs input{
		width:100%
	}
	.btn-otp{
		font-size:14px
	}
	.btn-resend-otp{
		font-size:14px;
	}
	.form-content-left {
	    margin-bottom: 30px;
	}
}
/*END OTP PASSWORD*/
</style>


<?php
// if(empty($_GET["code"])) {
//   wp_redirect(home_url()); exit;
// }

?>




<!--<div class="verification-code">-->
<!--  <label class="control-label">Verification Code</label>-->
<!--  <div class="verification-code--inputs">-->
<!--    <div>-->
<!--      <input type="text" maxlength="1" />-->
<!--      <input type="text" maxlength="1" />-->
<!--      <input type="text" maxlength="1" />-->
<!--      <input type="text" maxlength="1" />-->
<!--      <input type="text" maxlength="1" />-->
<!--      <input type="text" maxlength="1" />-->
<!--      <input type="hidden" id="verificationCode" name="otp" />-->
<!--      <input type="hidden" id="code" name="code" value="<?php $_GET["code"] ?>" />-->
<!--      <button class="btn-otp" type="submit" >gửi</button>-->
<!--      <button class="btn-resend-otp" type="button" >gửi lại mã</button>-->
<!--    </div>-->
<!--  </div>-->
  
<!--</div>-->
<div class="wrap-form">
  <div class="form-content-left">
    <div class="tit-wrap">
      <h3 class="tit">Nhập mã xác nhận</h3>
      <div class="content"> 
        Bằng việc chọn Tiếp tục, 
        tôi xác nhận đồng ý với Chính sách bảo mật và chia sẻ thông tin và Điều khoản & Điều kiện của Men O.logy 
        và Chính sách bảo mật và chia sẻ thông tin của website Men.Ologybynature.
      </div>
    </div>
    <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2">
      <!--<input class="m-2 text-center form-control rounded ap-otp-input" type="tel" id="first" maxlength="1" data-index="0" /> -->
      <!--<input class="m-2 text-center form-control rounded ap-otp-input" type="tel" id="second" maxlength="1" data-index="1" /> -->
      <!--<input class="m-2 text-center form-control rounded ap-otp-input" type="tel" id="third" maxlength="1" data-index="2" /> -->
      <!--<input class="m-2 text-center form-control rounded ap-otp-input" type="tel" id="fourth" maxlength="1" data-index="3" /> -->
      <!--<input class="m-2 text-center form-control rounded ap-otp-input" type="tel" id="fifth" maxlength="1" data-index="4" /> -->
      <!--<input class="m-2 text-center form-control rounded ap-otp-input" type="tel" id="sixth" maxlength="1" data-index="5" /> -->
      <input type="text" id="verificationCode" name="otp" />
    </div>
    <span class="error-noti"></span>
    <div class="btn-wrap">
      <button class="btn-otp" type="button" >
          Tiếp tục
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgba(241, 242, 243, 0); display: block; shape-rendering: auto;" width="20px" height="20px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
            <circle cx="50" cy="50" fill="none" stroke="#1eead6" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138">
              <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
            </circle>
        <!-- [ldio] generated by https://loading.io/ --></svg>
        <!-- [ldio] generated by https://loading.io/ --></svg>
      </button>
      <button id="resend-otp" onclick="resendOTP()" class="btn-resend-otp" type="button" >gửi lại mã <span id="countdown"></span></button>
    </div>
    
  </div>
  <div class="from-content-right" style="padding: 0">
		<!--<h3>Xin chào!</h3>-->
		<!--<p>-->
		<!--	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor neque,-->
		<!--	viverra blandit ullamcorper pulvinar enim, et. Enim nam odio ultrices tempus-->
		<!--	egestas neque, suspendisse sed fringilla. Sed at integer tristique in-->
		<!--	mattis.-->
		<!--</p>-->
		<img src="https://men.ologybynature.com/wp-content/uploads/2022/10/Hinh-dang-ky-dang-nha_WEB.png" style="width: 100%" height="100%" />
	</div>
</div>
<?php
get_footer();
?>

<script>
function resendOTP() {
    var timeleft = 60;
    var downloadTimer = setInterval(function(){
      if(timeleft <= 0){
        clearInterval(downloadTimer);
        document.getElementById("countdown").innerHTML = "";
        document.getElementById("resend-otp").disabled = false;
      } else {
        document.getElementById("resend-otp").disabled = true;
        document.getElementById("countdown").innerHTML = "(" + timeleft + ")";
        
      }
      timeleft -= 1;
    }, 1000);
}
//   var verificationCode = [];
//   var veri = 0;
//   jQuery(".verification-code input[type=text]").keyup(function (e) {
    
//     jQuery(".verification-code input[type=text]").each(function (i) {
//       verificationCode[i] = jQuery(".verification-code input[type=text]")[i].value; 
//       jQuery('#verificationCode').val(Number(verificationCode.join('')));
//     });

//     if (jQuery(this).val() > 0) {
//       if (event.key == 1 || event.key == 2 || event.key == 3 || event.key == 4 || event.key == 5 || event.key == 6 || event.key == 7 || event.key == 8 || event.key == 9 || event.key == 0) {
//         jQuery(this).next().focus();
//       }
//     }else {
//       if(event.key == 'Backspace'){
//           jQuery(this).prev().focus();
//       }
//     }

//   });

document.addEventListener("DOMContentLoaded", function (event) {
    
    
    
    
//   function OTPInput() {
//     let verificationCode = [];
    
//     const inputs = document.querySelectorAll("#otp > *[id]");
//     for (let i = 0; i < inputs.length; i++) {
//       inputs[i].addEventListener("keydown", function (event) {
//         if (event.key === "Backspace") {
//           inputs[i].value = "";
//           if (i !== 0) inputs[i - 1].focus();
//         } else {
//           if (i === inputs.length - 1 && inputs[i].value !== "") {
//             return true;
//           } else if (event.keyCode > 47 && event.keyCode < 58) {
//             inputs[i].value = event.key;
//             if (i !== inputs.length - 1) inputs[i + 1].focus();
//             event.preventDefault();
//           } else if (event.keyCode > 64 && event.keyCode < 91) {
//             inputs[i].value = String.fromCharCode(event.keyCode);
//             if (i !== inputs.length - 1) inputs[i + 1].focus();
//             event.preventDefault();
//           }
//         }
//         verificationCode[i] = inputs[i].value;
//         document.getElementById("verificationCode").value = Number(verificationCode.join(''));
//       });
//     }
//   }
//   OTPInput();


// const $inp = jQuery(".ap-otp-input");
// let full_otp = [];
// $inp.on({
//   paste(ev) {
//     // Handle Pasting

//     const clip = ev.originalEvent.clipboardData.getData("text").trim();
//     // Allow numbers only
//     if (!/\d{6}/.test(clip)) return ev.preventDefault(); // Invalid. Exit here
//     // Split string to Array or characters
//     const s = [...clip];
//     // Populate inputs. Focus last input.
//     $inp
//       .val((i) => s[i])
//       .eq(5)
//       .focus();
//     document.getElementById("verificationCode").value = Number(s.join(""));
//   },
//   input(ev) {
//     // Handle typing

//     const i = $inp.index(this);
//     if (this.value) $inp.eq(i + 1).focus();
//     full_otp[i] = this.value;
//     document.getElementById("verificationCode").value = Number(
//       full_otp.join("")
//     );
//   },
//   keydown(ev) {
//     // Handle Deleting

//     const i = $inp.index(this);
//     if (!this.value && ev.key === "Backspace" && i) $inp.eq(i - 1).focus();
//     full_otp[i] = this.value;
//     document.getElementById("verificationCode").value = Number(
//       full_otp.join("")
//     );
//   },
// });
});

  

</script>