(function ($) {
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split("&"),
      sParameterName,
      i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split("=");

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined
          ? true
          : decodeURIComponent(sParameterName[1]);
      }
    }
    return false;
  };
  $(document).ready(function () {
    $(".btn-otp").click(function () {
      let otp = $("#verificationCode").val();
      let code = getUrlParameter("code");
      console.log(otp)
      if(!otp) {
          $('.error-noti').empty().append("Chưa nhập mã OTP");
      } else {
          $.ajax({
            type: "POST",
            dataType: "json",
            url: devvn_array1.admin_ajax,
            data: {
              action: "ajax_otp",
              otp: otp,
              code: code,
              nonce: devvn_array1.ajax_otp_nonce,
            },
            beforeSend: function () {
              $(".btn-otp").addClass("active");
              $(".btn-otp").attr("disabled", true);
            },
            success: function (res) {
              if (res.success) {
                  
                window.location.href = "https://men.ologybynature.com/tai-khoan/";
                $(".btn-otp").removeClass("active");
                $(".btn-otp").attr("disabled", false);
              } else {
                  $('.error-noti').empty().append(res.data.mess);
                  $(".btn-otp").removeClass("active");
                $(".btn-otp").attr("disabled", false);
              }
              
            },
            error: function (res) {
              console.log("-200", res);
              $(".btn-otp").removeClass("active");
                $(".btn-otp").attr("disabled", false);
              
            },
          });
        }
    });
    $(".btn-resend-otp").click(function () {
      let code = getUrlParameter("code");
      $.ajax({
        type: "POST",
        dataType: "json",
        url: devvn_array1.admin_ajax,
        data: {
          action: "ajax_resend_otp",
          code: code,
          nonce: devvn_array1.ajax_otp_resend_nonce,
        },
        success: function (res) {
          console.log("200 rs", res);
          // if (res.status == 200) {
          //   window.location.href = "/";
          // }
        },
        error: function (res) {
          console.log("-200 rs", res);
        },
      });
    });
  });
})(jQuery);
