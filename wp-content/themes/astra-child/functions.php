<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );



add_action( "init",function(){
    // Set labels for custom_user
    $labels = array(
        "name" => "Custom Users",
        "singular_name" => "Custom User",
        "add_new"    => "Add Custom User",
        "add_new_item" => "Add New Custom User",
        "all_items" => "All Custom Users",
        "edit_item" => "Edit Custom User",
        "new_item" => "New Custom User",
        "view_item" => "View Custom User",
        "search_items" => "Search Custom Users",
    );
    // Set Options for custom_user
    $args = array(    
        "public" => true,
        "label"       => "Custom Users",
        "labels"      => $labels,
        "description" => "Custom Users custom post type.",
        "menu_icon"      => "dashicons-admin-post",    
        "supports"   => array( "title"),
        "capability_type" => "post",
        "publicly_queryable"  => true,
        "exclude_from_search" => false
    );
    register_post_type("custom_user", $args);
    
});


function vicode_errors(){
  static $wp_error; // global variable handle
  return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}

// displays error messages from form submissions
function vicode_register_messages() {
	if($codes = vicode_errors()->get_error_codes()) {
		echo '<div class="vicode_errors">';
		    // Loop error codes and display errors
		   foreach($codes as $code){
		        $message = vicode_errors()->get_error_message($code);
		        echo '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
		    }
		echo '</div>';
	}	
}


function redirect($url) {
  if(!empty($url)) {
    header("Location: {$url}");
  }
}

function getOTP($phone) {
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://otp.vietguys.biz:4438/api/index_otp.php',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => array('u' => 'menology','pwd' => 'qyk6y','from' => 'MEN O.LOGY','phone' => $phone,'otp' => '1','otp_expire' => '120','otp_max' => '-1'),
  ));
  
  $response = curl_exec($curl);
  
  curl_close($curl);
  return json_decode($response);
  
}


function verifyOTP($otp,$phone) {
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://otp.vietguys.biz:4438/api/verify_otp.php',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => array('u' => 'menology','pwd' => 'qyk6y','phone' => $phone,'otp' => $otp),
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  return json_decode($response);
}


add_action('admin_footer', 'my_admin_hide_cf');
function my_admin_hide_cf() {
    $u=wp_get_current_user();
    $user_roles = $u->roles;
    echo '
   <style>
   .acf-field-63520d273baa1 {display:none}
   </style>';
}


add_action( 'wp_ajax_ajax_otp', 'ajax_otp_func' );
add_action( 'wp_ajax_nopriv_ajax_otp', 'ajax_otp_func' );

function ajax_otp_func() {
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "ajax_otp_nonce")) {
        wp_send_json_error('None?');
	}
	
	header("Content-Type: application/json");
  
	$request= $_POST;
	if (!$request) {
		$request= $_GET;
	}

  $code = isset($request['code']) ? intval($request['code']) : '';
  $otp = $request['otp'];

    $data = [];
    $args = array(
      'p' => $code,
      'post_type' => 'custom_user',
    );
    $the_query_user = new WP_Query( $args );
    if ( $the_query_user->have_posts() ) {
      while ( $the_query_user->have_posts() ) {
        $the_query_user->the_post();
        $row = [];
        $data['user_login'] = get_field('cs_phone');
        $data['user_pass'] = get_field('cs_password');
        $data['display_name'] = get_the_title();
        $data['first_name'] = get_the_title();
        $data['user_registered']	= date('Y-m-d H:i:s');
        $data['role'] = 'subscriber'; 
      } 
    }

    $res = verifyOTP($otp,$data['user_login']);
    if($res->verified == 1) {
      $new_user_id = wp_insert_user($data);
      if($new_user_id) {
        // send an email to the admin
        wp_new_user_notification($new_user_id);
        
        // log the new user in
        wp_setcookie($data['user_login'], $data['user_pass'], true);
        wp_set_current_user($new_user_id, $data['user_login']);	
        do_action('wp_login', $data['user_login'],$data);
        
        // send the newly created user to the home page after logging them in
       wp_send_json_success(array(
          'status' => 200,
          'mess' => "login success"
        ), 200 );
      }
    } else {
      $response = [
        'status_code' => -1,
      ];
      if($res->error_code == 'WR_OTP') {
        $response['mess'] = 'Sai mã OTP';
      } elseif($res->error_code == 'OTP_EXPIRE') {
        $response['mess'] = 'Mã OTP hết hạn sử dụng';
      }
      wp_send_json_error($response);
    }
    die();
}



add_action( 'wp_ajax_ajax_resend_otp', 'ajax_otp_resend_func' );
add_action( 'wp_ajax_nopriv_ajax_resend_otp', 'ajax_otp_resend_func' );

function ajax_otp_resend_func() {
  if ( !wp_verify_nonce( $_REQUEST['nonce'], "ajax_otp_resend_nonce")) {
      wp_send_json_error('None?');
  }
	header("Content-Type: application/json");
  
	$request= $_POST;
	if (!$request) {
		$request= $_GET;
	}

  $code = isset($request['code']) ? intval($request['code']) : '';
$phone = get_field('cs_phone',$code);
    $data = [];
    $args = array(
      'p' => $code,
      'post_type' => 'custom_user',
    );
    $the_query_user = new WP_Query( $args );
    if ( $the_query_user->have_posts() ) {
      while ( $the_query_user->have_posts() ) {
        $the_query_user->the_post();
        $row = [];
        $data['user_login'] = get_field('cs_phone');
        $data['user_pass'] = get_field('cs_password');
        $data['display_name'] = get_the_title();
        $data['user_registered']	= date('Y-m-d H:i:s');
        $data['role'] = 'subscriber'; 
      } 
    }

    $res = getOTP($phone);
    if($res->error == 0 || $res->error == '0') {
      wp_send_json_success(array(
        'status' => 200,
        'mess' => "Tạo lại mã thành công",
      ), 200 );
    } else {
    //   wp_send_json_error($res);
    wp_send_json_error($phone);
    }
    die();
}



add_action( 'wp_enqueue_scripts', 'enqueue_UseAjaxInWp' );
function enqueue_UseAjaxInWp(){
    wp_enqueue_script( 'ajaxOTP', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'assets/js/ajax-otp.js' ), array( 'jquery' ), '1.0', true );
    $php_array = array(
        'admin_ajax'      => admin_url( 'admin-ajax.php'),
        'ajax_otp_nonce'   =>  wp_create_nonce('ajax_otp_nonce'),
        'ajax_otp_resend_nonce'   =>  wp_create_nonce('ajax_otp_resend_nonce'),
    );
    wp_localize_script( 'ajaxOTP', 'devvn_array1', $php_array );
}
function url_check_login() { 
    $url_login = home_url() . '/dang-nhap/';
    $url_account = home_url() . '/tai-khoan/';
    if(is_user_logged_in()) {
        return $url_account;
    } else {
        return $url_login;
    }
}
function desktop_account() { ?>
    <style>
        .btn-account-wrap {
            display: flex;
        }
        .tit-acc {
            font-size: 18px;
            text-transform: uppercase;
            font-weight: 500;
            color: #000;
            font-family: "TT Commons", Sans-serif;
            
        }
        .icon-acc {
            margin-right: 7px;
        }
        @media(max-width:767px){
            .tit-acc {
                display: none;
            }
            .icon-acc {
                margin-right: 0;
            }
        }
    </style>
    <div class="btn-account-wrap">
        <a href="<?php echo url_check_login(); ?>" class="icon-acc" >
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="none">
                <ellipse cx="16" cy="13.3333" rx="4" ry="4" stroke="black" stroke-width="1.5" stroke-linecap="round"></ellipse>
                <ellipse cx="16" cy="13.3333" rx="4" ry="4" stroke="black" stroke-width="1.5" stroke-linecap="round"></ellipse>
                <circle cx="16" cy="16" r="12" stroke="black" stroke-width="1.5"></circle>
                <circle cx="16" cy="16" r="12" stroke="black" stroke-width="1.5"></circle>
                <path d="M24 24.9412C23.5281 23.5237 22.4883 22.271 21.0419 21.3776C19.5955 20.4843 17.8232 20 16 20C14.1768 20 12.4046 20.4842 10.9581 21.3776C9.51167 22.271 8.47188 23.5236 8 24.9412" stroke="black" stroke-width="1.5" stroke-linecap="round"></path>
                <path d="M24 24.9412C23.5281 23.5237 22.4883 22.271 21.0419 21.3776C19.5955 20.4843 17.8232 20 16 20C14.1768 20 12.4046 20.4842 10.9581 21.3776C9.51167 22.271 8.47188 23.5236 8 24.9412" stroke="black" stroke-width="1.5" stroke-linecap="round"></path>
            </svg>
        </a>
        <a href="<?php echo url_check_login(); ?>" class="tit-acc">
            Tài khoản
        </a>
        
    </div>
<?php
	
}
add_shortcode( 'ongtt_account', 'desktop_account' );


add_shortcode('current_user_data','getUserData');

function getUserData(){
	$current_user = wp_get_current_user();
	return '<class class="data_area"><div class="user_name">'.$current_user->display_name.'</div>'.'<div class="user_phone">'.$current_user->user_login.'</div>'.'<div class="user_id">'.$current_user->ID.'</div>'.'</div>';
};

add_shortcode('isLogin','isLogin');

function isLogin(){
	$isLogin = is_user_logged_in();
	if ($isLogin){
		$text='isLogin';
	} else {
		$text = 'notLogin';
	};
	return '<span class="isLogin" style="display:none">'.$text.'</span>';
}









add_shortcode( 'get_slug', 'getSlug' );
function getSlug() {
	global $post;
    return '<div class="product_slug">'.$post->post_name.'</div>';
}

add_shortcode( 'get_cat', 'getCat' );
function getCat() {
	global $post;
	$post_id=$post->ID;
	$cats = wp_get_object_terms( $post_id, 'danh_muc_san_pham');
	$returnString='';
	foreach ($cats as $item){
		$returnString.=$item->name;
	
	};
    return '<div class="product_cats">'.$returnString.'</div>';
};

add_shortcode( 'get_category_news', 'getCatNew' );
function getCatNew() {
	global $post;
	$post_id=$post->ID;
	$cats = wp_get_object_terms( $post_id, 'category');
	$returnString='';
	foreach ($cats as $item){
		$returnString.=$item->name;
	
	};
    return '<div class="new_cats">'.$returnString.'</div>';
};

add_shortcode( 'get_id', 'getId' );
function getId() {
	global $post;
	$post_id=$post->ID;
	
    return '<div class="product_id">'.$post_id.'</div>';
};

add_shortcode( 'get_price', 'getPrice' );
function getPrice() {
	global $post;
	$post_id=$post->ID;
	$price = get_post_meta($post_id,'gia_sản_phẩm',true);
	
	
    return '<div class="product_price">'.$price.'</div>';
};

add_shortcode( 'get_mentioned_product', 'getMentionedProduct' );
function getMentionedProduct() {
	global $post;
	$post_id=$post->ID;
	$product_id = get_post_meta($post_id,'sản_phẩm_duợc_dề_cập',true);
    return '<div class="mentioned_product_id">'.$product_id.'</div>';
};


add_shortcode( 'get_mentioned_product_cats', 'getMentionedProductCats' );
function getMentionedProductCats() {
	global $post;
	$post_id=$post->ID;
	$product_id = get_post_meta($post_id,'sản_phẩm_duợc_dề_cập',true);
	$cats = wp_get_object_terms( $product_id, 'danh_muc_san_pham');
	$returnString='';
	foreach ($cats as $item){
		$returnString.=$item->name;
	
	};
    return '<div class="product_cats">'.$returnString.'</div>';
};
?>

