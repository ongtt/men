<style>
	.wrap-form{
		width: 1170px;
		max-width: 100%;
		margin: 100px auto 120px;
		display: flex;
	}
	.wrap-form * {
		font-family: 'TT Commons';
	}
	.wrap-form > div{
		width: 50%;
	}
	.wrap-form fieldset {
		display: flex;
		flex-wrap: wrap;
		align-items: center;
		justify-content: space-between;
		padding: 0;
		border: unset;
	}
	.ongttcode_form input.input{
		background: transparent !important;
		border: 1px solid #707070;
		border-radius: 8px;
		padding: 18px 32px !important;
	}
	.ongttcode_form input.input::placeholder {
		font-weight: 400;
		font-size: 20px;
		line-height: 24px;
		color: #707070;
	}
	.ongttcode_form .ongttcode_user, .ongttcode_form .ongttcode_user_phone{
		width: 100%;
		margin-bottom: 32px;
	}
	.ongttcode_form .password, .ongttcode_form .password_again{
		width: calc(50% - 17px);
		margin-bottom: 32px;
	}
	.ongttcode_form .password {
		margin-right: 16px;
	}
	.ongttcode_form .password_again{
		margin-left: 16px;
	}
	.ongttcode_form .submit {
		width: 100%;
		background: #046D2F;
		box-shadow: 0px 2px 50px rgba(4, 109, 47, 0.05);
		border-radius: 8px;
		font-weight: 700;
		font-size: 20px;
		line-height: 24px;
		color: #fff;
	}
	.ongttcode_form label{
		display: none;	
	}
	.form-content{
		margin-right: 16px;
	}
	.from-content-right{
		margin-left: 16px;
	}
	.wrap-form .ongttcode_header {
		font-weight: 600;
		font-size: 42px;
		line-height: 50px;
		color: #111111;
		text-align: center;
		text-transform: uppercase
	}
	.form-content p{
		font-weight: 400;
		font-size: 20px;
		line-height: 26px;
		color: #707070;
		text-align: center;
	}
	.ongttcode_form {
		margin-bottom: 0;
	}
	.from-content-right {
		background: #00000033;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		padding: 32px;
	}
	.from-content-right h3{
		font-weight: 600;
		font-size: 48px;
		line-height: 57px;
		letter-spacing: -0.01em;
		color: #FFFFFF;
	}
	.from-content-right p{
		font-weight: 500;
		font-size: 18px;
		line-height: 22px;
		text-align: center;
		color: #FFFFFF;
	}
	.login_more {
		width: 100%;
	}
	.tp_login{
		width: 100%;
		text-align: center;
		margin: 32px 0 40px;
	}
	.tp_login span{
		font-weight: 400;
		font-size: 20px;
		line-height: 24px;
		color: #707070;
	}
	.tp_login a{
		font-weight: 500;
		font-size: 18px;
		line-height: 22px;
		color: #046D2F;
		text-decoration: underline;
	}
	.or {
		font-weight: 500;
		font-size: 16px;
		line-height: 22px;
		color: #494949;
		margin: 0 auto;
		margin-bottom: 20px;
		width: 100%;
		text-align: center;
		display: block;
	}
	.social_login {
		display: flex;
		justify-content: space-between;
		margin-top: 20px;
	}
	.social_login .fb, .social_login .gg{
		height: 53px;
		gap: 4px;
		width: calc(50% - 16px);
		border-radius: 4px;
		overflow: hidden;
	}
	.social_login img{
		width: 100%;
		height: 100%;
		object-fit: cover;
		object-position: center center;
	}
	.error {
	    color: red;
	}
	
	

/*REGISTER REPONSIVE*/
@media(max-width:767px){
.wrap-form{
	flex-direction:column;
	padding:0 16px
}
.wrap-form >div{
	width:100%;
}
.wrap-form h3.ongttcode_header{
	font-size:28px;
	line-height:33px;
}
.wrap-form h3.ongttcode_header+p{
	font-size:14px;
	line-height:21px;
	margin-top:8px;
}
.wrap-form input.input{
	height:45px;
	font-size:14px;
	line-height:45px!important;
	padding:0 16px!important;
}
	.ongttcode_form .password, .ongttcode_form .password_again{
		width:100%;
		margin-right:0;
		margin-left:0;
	}
	.wrap-form input[type=submit]{
		padding:0;
		font-size:16px;
		height:43px;
	}
	.tp_login span{
		font-size:16px!important;
	}
	.tp_login >a{
		font-size:14px!important
	}
	.tp_login{
		margin-bottom:30px
	}
	.or{
		font-size:15px;
		font-weight:500;
		margin-bottom:-10px
	}
	.social_login{
		flex-direction:column;
		gap:12px
	}
	.social_login>div{
		width:100%!important;
		border-radius:8px!important;
	}
	.from-content-right{
		margin-left:0;
		margin-top:100px;
		padding:16px;
		min-height:400px;
	}
	.from-content-right h3{
		font-size:32px!important
	}
	.from-content-right h3 +p{
		font-size:14px;
		line-height:20px;
		width:100%
	}
}
/*END REGISTER*/
	
	
	
</style>
<?php
/**
 * Template Name: cs-register
 *
 * Displays the cs-register Template of the theme.
 */

get_header();
?>

<?php



    
    

    if (isset( $_POST["ongttcode_user_phone"] ) && wp_verify_nonce($_POST['ongttcode_csrf'], 'ongttcode-csrf')) {
      $fullname		= $_POST["ongttcode_name"];	
      $phone		  = $_POST["ongttcode_user_phone"];
      $user_pass		= $_POST["ongttcode_user_pass"];
      $pass_confirm 	= $_POST["ongttcode_user_pass_confirm"];
      
      // this is required for username checks
      require_once(ABSPATH . WPINC . '/registration.php');
      
      if(username_exists($phone)) {
          // Username already registered
          vicode_errors()->add('username_unavailable', __('Số điện thoại đã tồn tại!'));
      }
      if($user_pass == '') {
          // passwords do not match
          vicode_errors()->add('password_empty', __('Hãy nhập mật khẩu.'));
      }
      if($user_pass != $pass_confirm) {
          // passwords do not match
          vicode_errors()->add('password_mismatch', __('Mật khẩu không khớp.'));
      }
      if(strlen($user_pass) < 6) {
           vicode_errors()->add('password_length', __('Mật khẩu ít nhất 6 ký tự.'));
      }
      
      $errors = vicode_errors()->get_error_messages();
      
      // if no errors then cretate user
      if(empty($errors)) {
          
            $args_cs = array (
                'post_status' => 'publish',
                'post_type' => 'custom_user',
                'posts_per_page' => 1,
                'meta_query' => array(
                    array(
                        'key' => 'cs_phone',      
                        'value' => $phone,       
                        'compare' => 'LIKE',           
                    )
                ),
            );
          $the_query_cs = new WP_Query( $args_cs );
          
          $customer = $the_query_cs->posts;
          if($customer) {
              $customer_id = $customer[0]->ID;
          }
          
          $custom_user = array(
            'ID' => $customer_id ? $customer_id : 0,
            'post_content' => '',
            'post_title' => $fullname,
            'post_status' => 'publish',
            'post_author' => 1,
            'post_type' =>'custom_user'
          );

          $insert_user = wp_insert_post($custom_user);
          
          
          if($insert_user != 0) {
            update_field('cs_phone',$phone,$insert_user);
            update_field('cs_password',$user_pass,$insert_user);
          }
          
        $res = getOTP($phone);

          if($res->error == 0 || $res->error == '0') {
              
              $url = home_url() . '/verify-otp?code='. $insert_user .'';
              
              echo '<script type="text/javascript">location.replace(" '. $url .' "); </script>';
              
          } else {
              vicode_errors()->add('opt_error', __($res->log . ". Vui lòng thử lại!"));
          }
          
      }
  
  }
?>
<div class="wrap-form">
	<div class = "form-content">
<!-- 		<h3 class="ongttcode_header"><?php _e('Register New Account'); ?></h3> -->
		<h3 class="ongttcode_header">ĐĂNG KÝ</h3>
		<p>
			Đăng ký tạo tài khoản ngay để nhận thêm nhiều thông tin và ưu đãi hấp dẫn dành cho thành viên!


		</p>
		<div class="error-wrap">
		    <?php 
    		    // show any error messages after form submission
    		    vicode_register_messages();
    		?>
		</div>
		
		<form id="ongttcode_registration_form" class="ongttcode_form" action="" method="POST">
			<fieldset>
				<label for="ongttcode_user_Login">Tên</label>
				<input name="ongttcode_name" id="ongttcode_user" class="ongttcode_user input" type="text" required placeholder="Tên của bạn" />

				<label for="ongttcode_user_phone">Số điện thoại</label>
				<input name="ongttcode_user_phone" id="ongttcode_user_phone" class="ongttcode_user_phone input" type="text" required placeholder="Số điện thoại"/>
				<label for="password"><?php _e('Password'); ?></label>
				<input name="ongttcode_user_pass" minlength="6" id="password" class="password input" type="password" required placeholder="Mật khẩu"/>

				<label for="password_again"><?php _e('Password Again'); ?></label>
				<input name="ongttcode_user_pass_confirm" minlength="6" id="password_again" class="password_again input" type="password" required placeholder="Nhập lại mật khẩu"/>
				<input type="hidden" name="ongttcode_csrf" value="<?php echo wp_create_nonce('ongttcode-csrf'); ?>"/>
<!-- 				<input type="submit" value="<?php _e('Register Your Account'); ?>" class="submit"/> -->
				<input type="submit" value="Đăng ký" class="submit"/>
			</fieldset>
			
			<div class="login_more">
				<div class="tp_login">
					<span> Bạn đã có tài khoản? </span>
					<a href="/dang-nhap"> Đăng nhập ngay </a>
				</div>
				<span class="or">
					Hoặc
				</span>
				<div class="social_login">
					<div class="fb">
					    <?php echo do_shortcode('[nextend_social_login provider="facebook"]') ?>
					    <!--<img src="https://hathuong.asia/wp-content/uploads/2022/10/button.svg" alt="fb" />-->
					</div>
					<div class="gg">
					    <?php echo do_shortcode('[nextend_social_login provider="google"]') ?>
					    <!--<img src="https://hathuong.asia/wp-content/uploads/2022/10/button-gg.svg" alt="gg" />-->
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="from-content-right" style="padding: 0">
		<!--<h3>Xin chào!</h3>-->
		<!--<p>-->
		<!--	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor neque,-->
		<!--	viverra blandit ullamcorper pulvinar enim, et. Enim nam odio ultrices tempus-->
		<!--	egestas neque, suspendisse sed fringilla. Sed at integer tristique in-->
		<!--	mattis.-->
		<!--</p>-->
		<img src="https://men.ologybynature.com/wp-content/uploads/2022/10/Hinh-dang-ky-dang-nha_WEB.png" style="width: 100%" height="100%" />
	</div>
</div>
<script>
jQuery('#ongttcode_registration_form input[type=text]').on('change invalid', function() {
    var textfield = jQuery(this).get(0);
    textfield.setCustomValidity('');
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Nhập tên của bạn');  
    }
});
jQuery('#ongttcode_registration_form input[type=password]').on('change minlength', function() {
    var textfield = jQuery(this).get(0);
    textfield.setCustomValidity('');
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Mật khẩu ít nhất 6 ký tự');  
    }
});
</script>
<?php
get_footer();